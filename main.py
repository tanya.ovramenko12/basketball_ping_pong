import pygame
import sys
import random


class Ball:
    def __init__(self, x, y):
        self.image = pygame.image.load('ball.png')
        self.geometry = self.image.get_rect()
        self.geometry.x = x
        self.geometry.y = y

    def draw(self, screen):
        screen.blit(self.image, self.geometry)


class Rectangle:
    def __init__(self, x, y, h, w, color, current_shift):
        self.x = x
        self.y = y
        self.h = h
        self.w = w
        self.color = color
        self.current_shift = current_shift

    def key_reaction(self, a):
        if a == 0:
            self.current_shift = -1
        elif a == 1:
            self.current_shift = 1
        else:
            self.current_shift = 0

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, (self.x, self.y, self.w, self.h), 0)


def check_collision_up(ball, rec):
    if ball.geometry.y == 450 and rec.x - 50 <= ball.geometry.x <= rec.x + 150:
        return True
    else:
        return False


def check_collision_side(ball, rec):
    if (ball.geometry.x + 100 == rec.x or ball.geometry.x == rec.x + 200) and ball.geometry.y >= 450:
        return True
    else:
        return False


def abs(x):
    if x >= 0:
        return x
    else:
        return -x


def main():
    black = 0, 0, 0
    size = width, height = 800, 600
    pygame.init()
    screen = pygame.display.set_mode(size, pygame.RESIZABLE)
    basketball = Ball(350, 250)
    rec = Rectangle(300, 550, 50, 200, (255, 255, 255), 0)
    count = 0
    game_over = False
    dx = 0
    dy = 0
    if random.randint(0, 1) == 0:
        dx = -1
    else:
        dx = 1
    if random.randint(0, 1) == 0:
        dy = -1
    else:
        dy = 1
    while not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    rec.key_reaction(0)
                if event.key == pygame.K_d:
                    rec.key_reaction(1)
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a or event.key == pygame.K_d:
                    rec.key_reaction(2)
        screen.fill(black)
        basketball.draw(screen)
        rec.draw(screen)
        if basketball.geometry.y == 500:
            f = pygame.font.Font(None, 36)
            screen.blit(f.render("GAME OVER", True, (255, 255, 255)), (100, 100))
        a = pygame.font.Font(None, 24)
        screen.blit(a.render(f"Count: {count}", True, (255, 255, 255)), (5, 5))
        pygame.time.wait(10)
        pygame.display.flip()
        basketball.geometry.x += dx
        basketball.geometry.y += dy
        if count % 2 == 0 and count != 0:
            if dx > 0:
                dx += 1
            else:
                dx -= 1
            if dy > 0:
                dy += 1
            else:
                dy -= 1
        if basketball.geometry.y == 0:
            dy = -dy
        if basketball.geometry.x == 700 or basketball.geometry.x == 0:
            dx = -dx
        if basketball.geometry.y == 500:
            dx = 0
            dy = 0
            rec.current_shift = 0
        if rec.x <= 0:
            rec.x = 0
        if rec.x >= 600:
            rec.x = 600
        if check_collision_up(basketball, rec):
            dy = -dy
            count += 1
        if check_collision_side(basketball, rec):
            dy = -dy
            dx = -dx
            count += 1
        rec.x += rec.current_shift


if __name__ == '__main__':
    main()
